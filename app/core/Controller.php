<?php

/**
 * Created by IntelliJ IDEA.
 * User: DELL
 * Date: 06-Nov-15
 * Time: 3:09 PM
 *
 */
class Controller
{

	/**
	 * Controller constructor.
	 */
	public function __construct()
	{
		// initiate database
		$this->db = new Database();
	}

	public function model($model)
	{
		$file_path = '../app/models/' . $model . '.php';
		if (file_exists($file_path))
		{
			require_once '../app/models/' . $model . '.php';
			return new $model;
		}
	}

	public function view($view, $data)
	{
		require_once '../app/views/header.php';
		require_once '../app/views/' . $view . '.php';
		require_once '../app/views/footer.php';
	}
}