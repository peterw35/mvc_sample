<?php
/**
 * Created by IntelliJ IDEA.
 * User: DELL
 * Date: 06-Nov-15
 * Time: 4:21 PM
 */
define('DB_TYPE', 'pgsql');
define('DB_HOST', 'localhost');
define('DB_NAME', 'mvc');
define('DB_USER', 'postgres');
define('DB_PASS', 'w00y00');

class Database extends PDO {

	/**
	 * Database constructor.
	 */
	public function __construct()
	{
		parent::__construct(DB_TYPE.':host='.DB_HOST.';port=5432;dbname='.DB_NAME.';user='.DB_USER.';password='.DB_PASS);
	}
}