<?php

/**
 * Created by IntelliJ IDEA.
 * User: DELL
 * Date: 06-Nov-15
 * Time: 3:07 PM
 */
class App
{
	protected $controller = 'home';
	protected $method = 'index';
	protected $params = [];

	/**
	 * App constructor.
	 */
	public function __construct()
	{
		$url = $this->parseURL();
//        print_r($url);

		// Analyze url array elements:

		// call controllers ($url[0])
		$file_path = '../app/controllers/' . $url[0] . '.php';
		if (file_exists($file_path))
		{
			$this->controller = $url[0];
			unset($url[0]);
		}
		require_once '../app/controllers/' . $this->controller . '.php';
		$this->controller = new $this->controller;

//		echo $this->controller;

		// apply controller class methods
		if (isset($url[1]))
		{
			if (method_exists($this->controller, $url[1]))
			{
				$this->method = $url[1];
				unset($url[1]);
			}

		}

		$this->params = $url ? array_values($url) : [];

		// page redirect.
		call_user_func_array([$this->controller, $this->method], $this->params);

	}

	/**
	 * @return array[string]
	 */
	public function parseURL()
	{
		if (isset($_GET['url']))
		{
			$url = rtrim($_GET['url'], '/');
			$url = filter_var($url, FILTER_SANITIZE_URL);
			$url = explode('/', $url);

			return $url;
		}
	}
}