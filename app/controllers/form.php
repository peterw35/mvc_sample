<?php

/**
 * Created by IntelliJ IDEA.
 * User: DELL
 * Date: 06-Nov-15
 * Time: 4:15 PM
 */
class Form extends Controller
{
	public function index($status = '')
	{
		echo $status;
		$this->view('user_form/add_user', []);
	}

	public function add() {
		$form_model = $this->model('Form_Model');
		$form_model->add_user();
		if(!empty($form_model->error_msg))
		{
			$this->view('user_form/add_user', ['error'=>$form_model->error_msg]);
		}
		else
		{
			$this->view('user_form/add_user', []);
		}
	}
}