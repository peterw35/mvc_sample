<?php
/**
 * Created by IntelliJ IDEA.
 * User: DELL
 * Date: 06-Nov-15
 * Time: 3:11 PM
 */

class Home extends Controller
{
    public function index($name = '')
    {
        $this->view('home/index', ['name' => $name]);
    }
}